# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Open doors with your mobile app, via Mobile Key functionality! The SALTO KS mobile SDKs make it easy to build 
a seamless door opening experience in your application.

Decrease your app’s time to market with our out-of-box support for opening doors with your smartphone, 
natively inside your applications. Let’s save you some time, 
so you can work on the unique parts of your app that matter the most.

SALTO KS offers two types of credentials: Tag & Mobile Key

### TAG ###
A tag is a physical device (contactless key fob) that works based on RFID. 
Tags represent the user’s physical credentials to a lock. Every valid tag can lock and unlock a lock. 
RFID technologies available: DESfire, Mifare. 13.56MHz contactless RFID identification. 
High security, by using password and encrypted Proximity fobs.

### MOBILE KEY ###
A Mobile Key is the virtual equivalent of a Tag. The Mobile Key is stored on the user’s mobile device, 
and when presented to a lock, it uses BLE (Bluetooth Low Energy) to open the lock. 
This way users can use their smartphones as a Tag, opening the lock without relying on internet connection; 
which makes Mobile Key as secure as using a physical Tag.

### How do I get set up? ###

Requirements
Android SDK 21+

The Mobile Key feature, since it uses the BLE technology (Bluetooth Low Energy), 
requires permission for **ACCESS_COARSE_LOCATION** declared in the AndroidManifest.xml

Since Android 10 also **ACCESS_FINE_LOCATION** is required


### QUICKSTART ###
---

```java
IClaySDK claySDK = ClaySDK.init(context, API_PUBLIC_KEY, UUID); 
String publicKey = claySDK.getPublicKey(); 
String mkey = "<YOUR_MKEY>"; 

try { 
	claySDK.openDoor(mkey, new ILockDiscoveryCallback() { 
	
		@Override public void onPeripheralFound() { 
			// handle the onPeripheralFound event 
		} 
		
		@Override public void onSuccess(Result result) { 
			// handle the onSuccess event 
            
            int resultCode = result.getOpResult();
            
            // possible results at com.saltosystems.justinmobile.sdk.common.OpResult
		} 
		
		@Override public void onFailure(ClayException exception) { 
			// handle the onFailure event 
		} 
	}); 
} catch (ClayException e) { 
	// handle the exception 
}
```

